export class NumberFormatter {
    FormatNumber(number: number): string {
        return number.toLocaleString('pl-pl', {maximumFractionDigits: 2, minimumFractionDigits: 2});
    }
}
