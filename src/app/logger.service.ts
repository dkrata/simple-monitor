// https://robferguson.org/blog/2017/09/09/a-simple-logging-service-for-angular-4/
import { Injectable } from '@angular/core';

export enum LogLevel {
  Off = 0,
  Error = 1,
  Warning = 2,
  Info = 3,
  Debug = 4,
  All = 5
}

export abstract class Logger {
  info: any;
  debug: any;
  warn: any;
  error: any;
}

@Injectable()
export class LoggerService implements Logger {
  info: any;
  debug: any;
  warn: any;
  error: any;

  invokeConsoleMethod(type: string, args?: any): void {}
}
