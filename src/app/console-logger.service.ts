import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

import { Logger, LogLevel } from './logger.service';

export let isDebugMode = environment.isDebugMode;
export let logLevel = environment.logLevel;

const noop = (): any => undefined;

@Injectable()
export class ConsoleLoggerService implements Logger {

  get debug() {
    if (isDebugMode && logLevel >= LogLevel.Debug) {
      return console.debug.bind(console);
    } else {
      return noop;
    }
  }

  get info() {
    if (isDebugMode && logLevel >= LogLevel.Info) {
      return console.info.bind(console);
    } else {
      return noop;
    }
  }

  get warn() {
    if (isDebugMode && logLevel >= LogLevel.Warning) {
      return console.warn.bind(console);
    } else {
      return noop;
    }
  }

  get error() {
    if (isDebugMode && logLevel >= LogLevel.Error) {
      return console.error.bind(console);
    } else {
      return noop;
    }
  }
  
  invokeConsoleMethod(type: string, args?: any): void {
    const logFn: Function = (console)[type] || console.log || noop;
    logFn.apply(console, [args]);
  }
}