export enum HealthLevel {
    OK,
    WARNING,
    CRITICAL
}

export class Notification {
    hostname: string;
    message: string;
}

export enum ComponentType {
    Drive,
    Service,
    Custom
}

export class ComponentStatus {
    id: string;
    health: HealthLevel;
    type: ComponentType;
    hostname: string;
    message: string;
}

export class ServiceStatus extends ComponentStatus {
    name: string;
    username: string;
    status: string;
    state: string;
}

export class DriveStatus extends ComponentStatus {
    letter: string;
    sizeKB: number;
    freeSpaceKB: number;
    status: string;
}

export class CustomStatus extends ComponentStatus {
    name: string;
    statusDetails: string;
    status: string;
}

export class HostStatus {
    hostname: string;
    serverUpTime: {
        days: number;
        hours: number;
        minutes: number;
        seconds: number;
    };
    services: ServiceStatus[] = [];
    drives: DriveStatus[] = [];
    customs: CustomStatus[] = [];
    health: HealthLevel;
}
