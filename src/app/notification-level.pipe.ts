import { Pipe, PipeTransform } from '@angular/core';
import { ComponentStatus, HealthLevel } from './host-status-classes';
import { LoggerService } from './logger.service';

@Pipe({
  name: 'minNotificationLevel',
  pure: false
})
export class NotificationLevelPipe implements PipeTransform {

  constructor(private logger: LoggerService) { }

  transform(notifications: ComponentStatus[], minNotificationLevel: HealthLevel): ComponentStatus[] {
    this.logger.debug('Checking for min health level ' + minNotificationLevel + '. Notifications: ' + notifications.length);
    return notifications.filter(notification => notification.health >= minNotificationLevel);
  }

}
