import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { StatusesService } from '../statuses.service';
import { LoggerService } from '../logger.service';
import { Callbackable } from '../callbackable';
import { HostStatus, HealthLevel, ComponentStatus } from '../host-status-classes';
import { NumberFormatter } from '../number-formatter';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit, OnDestroy, Callbackable {
  private hostname: string;
  hostStatus: HostStatus;
  healthLevel = HealthLevel;
  numberFormatter = new NumberFormatter();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private statusesService: StatusesService,
    private logger: LoggerService
  ) { }

  ngOnInit() {
    this.hostname = this.route.snapshot.paramMap.get('hostname');
    this.logger.info(`Getting details for: ${this.hostname}`);
    this.hostStatus = this.statusesService.GetStatus();
    this.statusesService.SubscribeSelectedStatus(this, this.hostname);
  }

  ngOnDestroy() {
    this.statusesService.UnsubscribeSelectedStatus(this, this.hostname);
  }

  Callback() {
    this.logger.info('Updating selected status in DetailsComponent');
  }

  GetNotificationCount(components: ComponentStatus[], healthLevel: HealthLevel) : Number {
    let count = 0;
    components.forEach(component => {
      if (component.health === healthLevel) {
        count++;
      }
    });
    return count;
  }
}
