import { Component, OnInit, OnDestroy } from '@angular/core';
import { StatusesService } from './../statuses.service';
import { Callbackable } from '../callbackable';
import { HostStatus, HealthLevel } from '../host-status-classes';
import { LoggerService } from '../logger.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-status-list',
  templateUrl: './status-list.component.html',
  styleUrls: ['./status-list.component.css']
})
export class StatusListComponent implements OnInit, OnDestroy, Callbackable {
  hostStatuses: HostStatus[] = [];
  healthLevel = HealthLevel;

  constructor(
    private logger: LoggerService,
    private statusesService: StatusesService
  ) { }

  ngOnInit() {
    this.logger.debug('ngOnInit');
    this.hostStatuses = this.statusesService.GetStatuses();
    this.statusesService.SubscribeAllStatuses(this);
  }

  ngOnDestroy() {
    this.logger.debug('ngOnDestroy');
    this.statusesService.UnsubscribeAllStatuses(this);
  }

  Callback() {
    this.logger.debug('callback is called for StatusListComponent');
  }

  GetNotificationCount(hostStatus: HostStatus, healthLevel: HealthLevel): Number {
    let count = 0;
    hostStatus.services.forEach(service => {
      if (service.health === healthLevel) {
        count++;
      }
    });
    hostStatus.drives.forEach(drive => {
      if (drive.health === healthLevel) {
        count++;
      }
    });
    hostStatus.customs.forEach(item => {
      if (item.health === healthLevel) {
        count++;
      }
    });
    return count;
  }

  IsReloading(): boolean {
    return this.statusesService.IsReloading();
  }
}
