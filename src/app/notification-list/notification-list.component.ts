import { Component, OnInit } from '@angular/core';
import { StatusesService } from '../statuses.service';
import { LoggerService } from '../logger.service';
import { Notification, ComponentStatus, HostStatus, HealthLevel } from '../host-status-classes';

@Component({
  selector: 'app-notification-list',
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.css']
})
export class NotificationListComponent implements OnInit {

  notifications: ComponentStatus[] = [];
  hostStatuses: HostStatus[] = [];
  healthLevel = HealthLevel;
  showOk = false;
  showWarnings = true;
  showCriticals = true;

  constructor(private logger: LoggerService, private statusesService: StatusesService) { }

  ngOnInit() {
    this.logger.debug('ngOnInit');
    this.notifications = this.statusesService.GetNotifications();
    this.hostStatuses = this.statusesService.GetStatuses();
  }

  GetNotificationCount(notifications: ComponentStatus[], healthLevel: HealthLevel): Number {
    let count = 0;
    notifications.forEach(service => {
      if (service.health === healthLevel) {
        count++;
      }
    });
    return count;
  }

  ShowHideNotifications(healthLevel: HealthLevel): void {
    switch (healthLevel) {
      case HealthLevel.OK:
        this.showOk = !this.showOk;
        break;
      case HealthLevel.WARNING:
        this.showWarnings = !this.showWarnings;
        break;
      case HealthLevel.CRITICAL:
        this.showCriticals = !this.showCriticals;
        break;
    }
  }

  IsVisible(healthLevel: HealthLevel): boolean {
    switch (healthLevel) {
      case HealthLevel.OK:
        return this.showOk;
      case HealthLevel.WARNING:
        return this.showWarnings;
      case HealthLevel.CRITICAL:
        return this.showCriticals;
      default:
        return false;
    }
  }
}
