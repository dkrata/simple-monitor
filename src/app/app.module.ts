import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';

import { LoggerService } from './logger.service';
import { ConsoleLoggerService } from './console-logger.service';
import { AppComponent } from './app.component';
import { StatusesService } from './statuses.service';
import { HeaderComponent } from './header/header.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { StatusListComponent } from './status-list/status-list.component';
import { NotificationListComponent } from './notification-list/notification-list.component';
import { NotificationLevelPipe } from './notification-level.pipe';
import { DetailsComponent } from './details/details.component';
import { NumberFormatter } from './number-formatter';
import { NotificationSortPipe } from './notification-sort.pipe';

const appRoutes: Routes = [
  { path: 'dashboard', component: DashboardComponent },
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'details/:hostname', component: DetailsComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PageNotFoundComponent,
    DashboardComponent,
    StatusListComponent,
    NotificationListComponent,
    NotificationLevelPipe,
    DetailsComponent,
    NotificationSortPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  providers: [
    StatusesService,
    { provide: LoggerService, useClass: ConsoleLoggerService },
    NumberFormatter
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
