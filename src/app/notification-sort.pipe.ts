import { Pipe, PipeTransform } from '@angular/core';
import { ComponentStatus, HealthLevel } from './host-status-classes';
import { LoggerService } from './logger.service';

@Pipe({
  name: 'notificationSort',
  pure: false
})
export class NotificationSortPipe implements PipeTransform {

  constructor(private logger: LoggerService) { }

  transform(notifications: ComponentStatus[], orderDesc: boolean = true): ComponentStatus[] {
    this.logger.debug(`Order notifications : ${orderDesc ? 'DESC' : 'ASC'}`);
    return notifications.sort((a: ComponentStatus , b: ComponentStatus) => {
      const lower: number = orderDesc ? 1 : -1;
      const bigger: number = orderDesc ? -1 : 1;
      if (a.health > b.health) {
        return bigger;
      } else if (a.health < b.health) {
        return lower;
      }
      return 0;
    });
  }

}
