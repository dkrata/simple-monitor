export interface Callbackable {
    Callback: () => void;
}
