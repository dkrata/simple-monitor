import { Injectable } from '@angular/core';
import { LoggerService } from './logger.service';
import { Http } from '@angular/http';
import { Callbackable } from './callbackable';
import { HostStatus, HealthLevel, ServiceStatus, Notification, DriveStatus, ComponentStatus, ComponentType, CustomStatus } from './host-status-classes';
import { Services } from '@angular/core/src/view';
import { NumberFormatter } from './number-formatter';

@Injectable()
export class StatusesService {

  private pathesPrefix = '/assets/scanResults/';
  private resultFiles: string = this.pathesPrefix + 'resultFiles.json';
  private pathesToWatch: string[] = [];
  private pathesToWatchProcessed = false;

  private isReloading = false;
  private reloadedItemsCount = 0;
  private updateHostStatusesIntervalId = -1;
  private startWatchAllHostsIntervalId = -1;
  private allHostsWatchingStarted = false;
  private allHostsWatchingIntervalMS: number = 20 * 1000; // 20s
  private watchedHostsStatuses: HostStatus[] = [];
  private watchedNotifications: ComponentStatus[] = [];

  private isReloadingSelected = false;
  private selectedPathToWatch: string;
  private selectedHostWatchingStarted = false;
  private updateHostStatusIntervalId = -1;
  private startWatchSelectedHostsIntervalId = -1;
  private watchedHostIntervalMS: number = 20 * 1000; // 20s
  private watchedHostStatus: HostStatus = new HostStatus();

  private statusesSubscribers: Callbackable[] = [];
  private statusSubscribers: Callbackable[] = [];

  constructor(private http: Http, private logger: LoggerService, private numberFormatter: NumberFormatter) {
    this.logger.debug('Constructor');
    this.setResultFilesPathes();
  }

  //#region Private methods
  private setResultFilesPathes(): void {
    this.logger.debug('setResultFilesPathes');
    this.http
      .get(this.resultFiles)
      .toPromise()
      .then(
        this.resultFilesPathesReceived.bind(this),
        this.resultFilesPathesReceiveError.bind(this)
    );
  }

  private resultFilesPathesReceived(result): void {
    this.logger.info('Pathes to watch are received');
    this.logger.debug(result.json());
    const files: string[] = result.json();
    files.forEach(file => {
      this.pathesToWatch.push(this.pathesPrefix + file);
      this.logger.debug('Added path: ' + this.pathesPrefix + file);
    });
    this.logger.info('Pathes to watch are set');
    this.pathesToWatchProcessed = true;
  }

  private resultFilesPathesReceiveError(result): void {
    this.logger.error('Pathes to watch are not received', result);
  }

  private updateHostsStatuses(): void {
    this.logger.debug('updateHostsStatuses');
    if (this.pathesToWatchProcessed === false) {
      this.logger.warn('Pathes to watch are not processed yet');
      return;
    }
    this.reloadedItemsCount = 0;
    this.isReloading = true;
    this.pathesToWatch.forEach(path => {
      this.logger.info('Request to update ' + path);
      this.http
        .get(path)
        .toPromise()
        .then(result => {
          this.logger.info('HostStatuses: Received update for ' + path);
          const hostStatus: HostStatus = result.json();
          this.setNotifications(hostStatus);
          const oldHostStatus: HostStatus = this.watchedHostsStatuses.find((currentHostStatus: HostStatus) => {
            return hostStatus.hostname === currentHostStatus.hostname;
          });
          this.updateHostStatusInArray(oldHostStatus, hostStatus);
          this.updateNotificationArray(hostStatus);
          this.reloadedItemsCount++;
          this.logger.debug('Reloaded items: ' + this.reloadedItemsCount);
          if (this.reloadedItemsCount === this.pathesToWatch.length) {
            this.logger.info('Reloading statuses is done');
            this.isReloading = false;
          }
        });
    });
  }

  private updateHostStatus(): void {
    this.logger.debug('updateHostsStatus');
    if (this.pathesToWatchProcessed === false) {
      this.logger.warn('Pathes to watch are not processed yet');
      return;
    }
    this.isReloadingSelected = true;
    this.logger.info('Request to update ' + this.selectedPathToWatch);
    this.http
      .get(this.selectedPathToWatch)
      .toPromise()
      .then(result => {
        this.logger.info('HostStatus: Received update for ' + this.selectedPathToWatch);
        const hostStatus: HostStatus = result.json();
        this.setNotifications(hostStatus);
        this.updateSelectedHostStatus(this.watchedHostStatus, hostStatus);
        this.logger.info('Reloading status is done');
        this.isReloadingSelected = false;
      });
  }

  private setNotifications(hostStatus: HostStatus) {
    this.logger.info('setNotifications');
    hostStatus.drives.forEach((drive: DriveStatus) => {
      this.setDriveStatus(drive, hostStatus.hostname);
    });
    hostStatus.services.forEach((service: ServiceStatus) => {
      this.setServiceStatus(service, hostStatus.hostname);
    });
    hostStatus.customs.forEach((custom: CustomStatus) => {
      this.setCustomStatus(custom, hostStatus.hostname);
    });
    let healthLevel: HealthLevel = HealthLevel.OK;
    if (hostStatus.drives.findIndex(item => item.health === HealthLevel.CRITICAL) >= 0
      || hostStatus.services.findIndex(item => item.health === HealthLevel.CRITICAL) >= 0
      || hostStatus.customs.findIndex(item => item.health === HealthLevel.CRITICAL) >= 0) {
      healthLevel = HealthLevel.CRITICAL;
    } else if (hostStatus.drives.findIndex(item => item.health === HealthLevel.WARNING) >= 0
      || hostStatus.services.findIndex(item => item.health === HealthLevel.WARNING) >= 0
      || hostStatus.customs.findIndex(item => item.health === HealthLevel.WARNING) >= 0) {
      healthLevel = HealthLevel.WARNING;
    }
    hostStatus.health = healthLevel;
  }

  private updateSelectedHostStatus(oldHostStatus: HostStatus, updatedHostStatus: HostStatus) {
    this.logger.debug('updateSelectedHostStatus');
    if (!oldHostStatus) {
      this.logger.info('Status is set: ' + updatedHostStatus.hostname);
      this.watchedHostStatus = updatedHostStatus;
      return;
    }

    this.logger.info('Updating status for: ' + oldHostStatus.hostname);
    this.updateHostStatusObject(oldHostStatus, updatedHostStatus);
  }

  private updateHostStatusInArray(oldHostStatus: HostStatus, updatedHostStatus: HostStatus) {
    this.logger.debug('updateHostStatusInArray');
    if (!oldHostStatus) {
      this.logger.info(`New status to add: ${updatedHostStatus.hostname}. Statuses before: ${this.watchedHostsStatuses.length}`);
      this.watchedHostsStatuses.push(updatedHostStatus);
      this.logger.info(`Statuses after add: ${this.watchedHostsStatuses.length}`);
      return;
    }

    this.logger.info('Updating status for: ' + oldHostStatus.hostname);
    this.updateHostStatusObject(oldHostStatus, updatedHostStatus);
    this.logger.debug(`Updated ${updatedHostStatus.hostname}. Current count = ${this.watchedHostsStatuses.length}`);
  }

  private updateHostStatusObject(oldHostStatus: HostStatus, updatedHostStatus: HostStatus) {
    this.logger.debug('updateHostStatusObject');
    oldHostStatus.hostname = updatedHostStatus.hostname;
    oldHostStatus.serverUpTime = updatedHostStatus.serverUpTime;
    oldHostStatus.health = updatedHostStatus.health;

    this.updateStatuses(oldHostStatus.services, updatedHostStatus.services, ComponentType.Service);
    this.updateStatuses(oldHostStatus.drives, updatedHostStatus.drives, ComponentType.Drive);
    this.updateStatuses(oldHostStatus.customs, updatedHostStatus.customs, ComponentType.Custom);
  }

  private updateStatuses(oldStatuses: ComponentStatus[], updatedStatuses: ComponentStatus[], componentType: ComponentType) {
    this.logger.debug('updateStatues');
    oldStatuses.forEach(oldService => {
      const statusIndex = updatedStatuses.findIndex(service => {
        return service.id === oldService.id;
      });
      if (statusIndex === -1) {
        oldStatuses.splice(statusIndex);
      }
    });
    updatedStatuses.forEach(updatedStatus => {
      const statusIndex = oldStatuses.findIndex(service => {
        return service.id === updatedStatus.id;
      });
      if (statusIndex !== -1) {
        switch (componentType) {
          case ComponentType.Service:
            this.updateService(<ServiceStatus>oldStatuses[statusIndex], <ServiceStatus>updatedStatus);
            break;
          case ComponentType.Drive:
            this.updateDrive(<DriveStatus>oldStatuses[statusIndex], <DriveStatus>updatedStatus);
            break;
          case ComponentType.Custom:
            this.updateCustom(<CustomStatus>oldStatuses[statusIndex], <CustomStatus>updatedStatus);
            break;
        }
      } else {
        oldStatuses.push(updatedStatus);
      }
    });
  }

  private updateService(oldStatus: ServiceStatus, updatedStatus: ServiceStatus): void {
    oldStatus.health = updatedStatus.health;
    oldStatus.state = updatedStatus.state;
    oldStatus.status = updatedStatus.status;
    oldStatus.username = updatedStatus.username;
    oldStatus.message = updatedStatus.message;
  }

  private updateDrive(oldStatus: DriveStatus, updatedStatus: DriveStatus): void {
    oldStatus.health = updatedStatus.health;
    oldStatus.freeSpaceKB = updatedStatus.freeSpaceKB;
    oldStatus.sizeKB = updatedStatus.sizeKB;
    oldStatus.status = updatedStatus.status;
    oldStatus.message = updatedStatus.message;
  }

  private updateCustom(oldStatus: CustomStatus, updatedStatus: CustomStatus): void {
    oldStatus.health = updatedStatus.health;
    oldStatus.status = updatedStatus.status;
    oldStatus.statusDetails = updatedStatus.status;
    oldStatus.message = updatedStatus.message;
  }

  private updateNotificationArray(hostStatus: HostStatus): void {
    // Delete old status if not exist in update. Update / add current status
    this.removeNotWatchedComponents(hostStatus.hostname, hostStatus.drives, ComponentType.Drive);
    this.updateWatchedComponents(hostStatus.drives);
    this.removeNotWatchedComponents(hostStatus.hostname, hostStatus.services, ComponentType.Service);
    this.updateWatchedComponents(hostStatus.services);
    this.removeNotWatchedComponents(hostStatus.hostname, hostStatus.customs, ComponentType.Custom);
    this.updateWatchedComponents(hostStatus.customs);
  }

  private removeNotWatchedComponents(hostname: string, componentStatuses: ComponentStatus[], componentType: ComponentType): void {
    const indexesToDelete: number[] = [];
    this.watchedNotifications.forEach((watchedStatus: ComponentStatus) => {
      if (watchedStatus.hostname !== hostname || watchedStatus.type !== componentType) {
        return;
      }
      const index = componentStatuses.findIndex((componentStatus: ComponentStatus) => {
        return componentStatus.id === watchedStatus.id;
      });
      if (index === -1) {
        indexesToDelete.push(this.watchedNotifications.indexOf(watchedStatus));
      }
    });
    indexesToDelete.reverse();
    indexesToDelete.forEach(index => {
      this.logger.debug(`Component ${this.watchedNotifications[index].id} is healthy or not exist in: ${hostname}`);
      this.watchedNotifications.splice(index);
    });
  }

  private updateWatchedComponents(componentStatuses: ComponentStatus[]) {
    componentStatuses.forEach(updatedStatus => {
      const statusIndex = this.watchedNotifications.findIndex(status => {
        return status.id === updatedStatus.id;
      });
      if (statusIndex !== -1) {
        this.watchedNotifications[statusIndex].health = updatedStatus.health;
        this.watchedNotifications[statusIndex].message = updatedStatus.message;
        this.logger.debug('Component status updated: ' + updatedStatus.id);
      } else {
        this.watchedNotifications.push(updatedStatus);
        this.logger.debug('New component status added: ' + updatedStatus.id);
      }
    });
  }

  private setDriveStatus(drive: DriveStatus, hostname: string) {
    drive.hostname = hostname;
    drive.id = hostname + '_drive_' + drive.letter;
    drive.type = ComponentType.Drive;

    const freeSpaceMB = this.numberFormatter.FormatNumber(drive.freeSpaceKB ? drive.freeSpaceKB / 1024 : 0);
    const driveSizeMB = this.numberFormatter.FormatNumber(drive.sizeKB / 1024);

    switch (drive.status.toLowerCase()) {
      case 'healthy':
        drive.health = HealthLevel.OK;
        drive.message = `Drive ${drive.letter} is healthy (${freeSpaceMB} MB of free space). Total space: ${driveSizeMB} MB`;
        break;
      case 'warning':
        drive.health = HealthLevel.WARNING;
        drive.message = `Drive ${drive.letter} free space is low (${freeSpaceMB} MB of free space). Total space: ${driveSizeMB} MB`;
        break;
      case 'critical':
        drive.health = HealthLevel.CRITICAL;
        drive.message = `Drive ${drive.letter} is out of space. Total space: ${driveSizeMB} MB`;
        break;
    }
  }

  private setServiceStatus(service: ServiceStatus, hostname: string) {
    service.hostname = hostname;
    service.id = hostname + '_service_' + service.name;
    service.type = ComponentType.Service;

    switch (service.state.toLowerCase()) {
      case 'running':
        service.health = HealthLevel.OK;
        service.message = `Service ${service.name} is working properly.`;
        break;
      case 'stopped':
        service.health = HealthLevel.CRITICAL;
        service.message = `Service ${service.name} is stopped.`;
        break;
      default:
        service.health = HealthLevel.WARNING;
        service.message = `Service ${service.name} is in not recognized state.`;
        break;
    }
  }

  private setCustomStatus(custom: CustomStatus, hostname: string) {
    custom.hostname = hostname;
    custom.id = hostname + '_custom_' + custom.name;
    custom.type = ComponentType.Custom;

    switch (custom.status.toLowerCase()) {
      case 'ok':
        custom.health = HealthLevel.OK;
        custom.message = `Custom '${custom.name}' is in health state. ${custom.statusDetails}.`;
        break;
      case 'critical':
        custom.health = HealthLevel.CRITICAL;
        custom.message = `Custom '${custom.name}' is in critical state. ${custom.statusDetails}.`;
        break;
      default:
        custom.health = HealthLevel.WARNING;
        custom.message = `Custom '${custom.name}' is in warning state. ${custom.statusDetails}.`;
        break;
    }
  }

  private startWatchSelectedHost(hostname: string): void {
    this.logger.debug(`Function: startWatchSelectedHost`);
    if (this.selectedHostWatchingStarted) {
      this.logger.warn(`Selected host is already watched. Currently: ${this.selectedPathToWatch.substring(this.pathesPrefix.length)}`);
      return;
    }

    if (!this.pathesToWatchProcessed) {
      if (this.startWatchSelectedHostsIntervalId >= 0) {
        this.logger.debug('startWatchSelectedHostsIntervalId already set');
        return;
      }
      this.startWatchSelectedHostsIntervalId = setInterval(this.startWatchSelectedHost.bind(this, hostname), 50);
      return;
    }
    clearInterval(this.startWatchSelectedHostsIntervalId);

    const pathToWatch = this.pathesToWatch.find(path => path.substring(this.pathesPrefix.length) === `${hostname}.json` );
    this.logger.debug(`Find path to watch: ${pathToWatch}`);
    if (!pathToWatch) {
      this.logger.error(`Cannot subscribe ${hostname} because it does not exists`);
      return;
    }
    this.selectedPathToWatch = pathToWatch;
    this.updateHostStatus();
    this.updateHostStatusIntervalId = setInterval(this.updateHostStatus.bind(this), this.watchedHostIntervalMS);
    this.selectedHostWatchingStarted = true;
  }

  private stopWatchSelectedHost(hostname: string): void {
    this.logger.debug(`Function: stopWatchSelectedHost`);
    if (!this.selectedHostWatchingStarted) {
      this.logger.warn('Selected host watching is already stopped');
      return;
    }

    clearInterval(this.updateHostStatusIntervalId);
    this.selectedPathToWatch = null;
    this.selectedHostWatchingStarted = false;
    this.watchedHostStatus = new HostStatus;
    this.logger.info('Selected host watching is stopped');
  }

  private startWatchAllHosts(): void {
    this.logger.debug(`Function: startWatchAllHosts`);
    if (this.allHostsWatchingStarted) {
      this.logger.warn('All hosts watching already started');
      return;
    }

    if (!this.pathesToWatchProcessed) {
      if (this.startWatchAllHostsIntervalId >= 0) {
        this.logger.debug('startWatchAllHostsInterval already set');
        return;
      }
      this.startWatchAllHostsIntervalId = setInterval(this.startWatchAllHosts.bind(this), 50);
      return;
    }
    clearInterval(this.startWatchAllHostsIntervalId);

    this.updateHostsStatuses();
    this.updateHostStatusesIntervalId = setInterval(this.updateHostsStatuses.bind(this), this.allHostsWatchingIntervalMS);
    this.allHostsWatchingStarted = true;
    this.logger.info('All hosts watching started');
  }
  //#endregion

  //#region Public methods
  GetStatuses(): HostStatus[] {
    this.logger.debug('GetStatuses');
    this.updateHostsStatuses();
    return this.watchedHostsStatuses;
  }

  GetStatus(): HostStatus {
    this.logger.debug('GetStatus');
    this.updateHostStatus();
    return this.watchedHostStatus;
  }

  GetNotifications(): ComponentStatus[] {
    this.logger.debug('GetNotifications');
    this.startWatchAllHosts();
    return this.watchedNotifications;
  }

  IsReloading(): boolean {
    return this.isReloading;
  }

  SubscribeSelectedStatus(callbackObject: Callbackable, selected: string): void {
    this.logger.debug(`Function: SubscribeSelectedStatus`);
    this.startWatchSelectedHost(selected);
    this.logger.info(`Subscriber for ${selected} added`);
    return;
  }

  UnsubscribeSelectedStatus(callbackObject: Callbackable, selected: string): void {
    this.logger.debug(`Function: UnsubscribeSelectedStatus`);
    this.stopWatchSelectedHost(selected);
    this.logger.info(`Subscriber for ${selected} deleted`);
    return;
  }

  SubscribeAllStatuses(callbackObject: any): void {
    this.logger.debug(`Function: SubscribeAllStatuses`);
    this.statusesSubscribers.push(callbackObject);
    this.startWatchAllHosts();
    this.logger.info('Subscriber for all added');
  }

  UnsubscribeAllStatuses(callbackObject: any): void {
    this.logger.debug(`Function: UnsubscribeAllStatuses`);
    const index: number = this.statusesSubscribers.findIndex((object) => {
      return object === callbackObject;
    });
    if (index < 0) {
      this.logger.error('Error - try to unsubscribe non-existing object');
      return;
    }
    this.statusesSubscribers.splice(index, 1);
    this.logger.info('Subscriber deleted');
    if (this.statusesSubscribers.length === 0) {
      this.allHostsWatchingStarted = false;
      clearInterval(this.updateHostStatusesIntervalId);
      this.logger.info('Statuses watching is stopped');
    }
  }
  //#endregion
}
